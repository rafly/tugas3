package com.example.tugas_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView myImageViewEyebrow;
    private ImageView myImageViewHair;
    private ImageView myImageViewMoustache;
    private ImageView myImageViewBeard;

    private CheckBox myCheckBoxEyebrow;
    private CheckBox myCheckBoxHair;
    private CheckBox myCheckBoxMoustache;
    private CheckBox myCheckBoxBeard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myImageViewEyebrow = findViewById(R.id.eyebrow);
        myImageViewHair = findViewById(R.id.hair);
        myImageViewMoustache = findViewById(R.id.moustache);
        myImageViewBeard = findViewById(R.id.beard);

        myCheckBoxEyebrow = findViewById(R.id.checkAlis);
        myCheckBoxHair = findViewById(R.id.checkRambut);
        myCheckBoxMoustache = findViewById(R.id.checkKumis);
        myCheckBoxBeard = findViewById(R.id.checkJanggut);

        myCheckBoxEyebrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myCheckBoxEyebrow.isChecked()) {
                    myImageViewEyebrow.setVisibility(View.VISIBLE);
                } else {
                    myImageViewEyebrow.setVisibility(View.INVISIBLE);
                }
            }
        });

        myCheckBoxHair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myCheckBoxHair.isChecked()) {
                    myImageViewHair.setVisibility(View.VISIBLE);
                } else {
                    myImageViewHair.setVisibility(View.INVISIBLE);
                }
            }
        });

        myCheckBoxMoustache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myCheckBoxMoustache.isChecked()) {
                    myImageViewMoustache.setVisibility(View.VISIBLE);
                } else {
                    myImageViewMoustache.setVisibility(View.INVISIBLE);
                }
            }
        });

        myCheckBoxBeard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myCheckBoxBeard.isChecked()) {
                    myImageViewBeard.setVisibility(View.VISIBLE);
                } else {
                    myImageViewBeard.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}